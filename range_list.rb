class RangeList
  attr_accessor :list

  def initialize()
    @list = []
  end  

  def get_range_index(range)
    range_start = range.first
    range_end = range.last

    list_lap_start = 0
    list_lap_end = 0
    self.list.each_with_index do |list_men,i|
      if list_men >= range_start && list_lap_start == 0
        #找到list中第一个大于等于range_start的位置
        list_lap_start = i
        if list_men > range_end
          list_lap_end = i
          break
        end
      elsif list_men > range_end
        #找到list中第一个比range_end大的位置
        list_lap_end = i
        break  
      end
    end
    return list_lap_start,list_lap_end
  end

  def add(range)
    range_start = range.first
    range_end = range.last

    list_start = self.list.first.to_i
    list_end = self.list.last.to_i

    ############### 在list头部添加 ###############
    if range_start <= list_start
      if range_end < list_start
        # 直接在头部添加
        self.list = range + self.list
      else
        # 替换头部参数
        self.list[0] = range_start
      end
      return self.list
    end

    ############### 在list尾部追加 ###############
    if list_end <= range_end
      if list_end < range_start
        # 直接在尾部追加
        self.list += range
      else
        # 替换尾部参数
        self.list[-1] = range_end
      end
      return self.list
    end

    ############### 在list中间追加 ###############
    list_lap_start , list_lap_end = get_range_index(range)

    # range落在闭区间内，不需要做处理(偶偶)
    if (list_lap_start+1)%2 == 0 && (list_lap_end+1)%2 == 0 && list_lap_start == list_lap_end
      return self.list
    end

    # range横跨一个开区间,把两头接上，即去掉中间开区间(偶偶)
    if (list_lap_start+1)%2 == 0 && (list_lap_end+1)%2 == 0
      self.list[list_lap_start..list_lap_end-1] = []
      return self.list
    end
   
    # range落在开区间内,直接插入中间(奇奇)
    if (list_lap_start+1)%2 != 0 && (list_lap_end+1)%2 != 0
      self.list[list_lap_start-1..list_lap_start] = [self.list[list_lap_start-1],range_start,range_end,self.list[list_lap_start]]
      return self.list
    end

    if (list_lap_start+1)%2 == 0
      # range_start落在一个闭区间内，range_end落在一个开区间内(偶奇)
      self.list[list_lap_start] = range_end
      return self.list
    else
      # range_start落在一个开区间内，range_end落在一个闭区间内(奇偶)
      self.list[list_lap_start] = range_start
      return self.list
    end

  end

  def remove(range)
    range_start = range.first
    range_end = range.last

    list_start = self.list.first.to_i
    list_end = self.list.last.to_i

    ############### 在list头部移除 ###############
    if range_start <= list_start
      # self.list[0] = Max(range_end,list_start)
      if range_end < list_start
        self.list[0] = list_start
      else
        self.list[0] = range_end
      end
      return self.list
    end

    ############### 在list尾部移除 ###############
    if list_end <= range_end
      # self.list[-1] = Min(list_end,range_start)
      if list_end < range_start
        self.list[-1] = list_end
      else
        self.list[-1] = range_start
      end
      return self.list
    end

    ############### 在list中间移除 ###############
    list_lap_start , list_lap_end = get_range_index(range)

    # range落在闭区间内(偶偶)
    if (list_lap_start+1)%2 == 0 && (list_lap_end+1)%2 == 0 && list_lap_start == list_lap_end
      if range_start == self.list[list_lap_start] && range_end == self.list[list_lap_end]
        # 全包含
        self.list[list_lap_start-1..list_lap_start] = []
      elsif range_start == self.list[list_lap_start-1]
        # 左起点重合
        self.list[list_lap_start-1] = range_end
      elsif range_end == self.list[list_lap_end-1]
        # 右终点重合
        self.list[list_lap_end-1] = range_start
      else
        # 在闭区间中间
        self.list[list_lap_start-1..list_lap_start] = [self.list[list_lap_start-1],range_start,range_end,self.list[list_lap_start]].uniq
      end
      return self.list
    end

    # range横跨一个开区间,替换开区间(偶偶)
    if (list_lap_start+1)%2 == 0 && (list_lap_end+1)%2 == 0
      self.list[list_lap_start..list_lap_end-1] = range
      return self.list
    end
   
    # range落在开区间内,移除中间内容(奇奇)
    if (list_lap_start+1)%2 != 0 && (list_lap_end+1)%2 != 0
      self.list[list_lap_start-1..list_lap_end] = []
      return self.list
    end

    if (list_lap_start+1)%2 == 0
      # range_start落在一个闭区间内，range_end落在一个开区间内(偶奇)
      self.list[list_lap_start] = range_start
      return self.list
    else
      # range_start落在一个开区间内，range_end落在一个闭区间内(奇偶)
      self.list[list_lap_start] = range_end
      return self.list
    end

  end

  def print
    # 组装打印
    p self.list.each_slice(2).to_a.to_s[1..-2].gsub("], [",") [").gsub("]",")")
  end

end

rl = RangeList.new
rl.add([1, 5])
rl.print
#// Should display: [1, 5)
rl.add([10, 20])
rl.print
#// Should display: [1, 5) [10, 20)
rl.add([20, 20])
rl.print
#// Should display: [1, 5) [10, 20)
rl.add([20, 21])
rl.print
#// Should display: [1, 5) [10, 21)
rl.add([2, 4])
rl.print
#// Should display: [1, 5) [10, 21)
rl.add([3, 8])
rl.print
#// Should display: [1, 8) [10, 21)
rl.remove([10, 10])
rl.print
#// Should display: [1, 8) [10, 21)
rl.remove([10, 11])
rl.print
#// Should display: [1, 8) [11, 21)
rl.remove([15, 17])
rl.print
#// Should display: [1, 8) [11, 15) [17, 21)
rl.remove([3, 19])
rl.print
#// Should display: [1, 3) [19, 21)